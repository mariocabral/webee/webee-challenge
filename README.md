
# [Webee](https://www.webee.io) Challenge

This project resolve the webee challenge to apply a job position.


## Setup

- Download the project:
```shell
git clone https://gitlab.com/mariocabral/webee/webee-challenge.git
cd webee-challenge
```
- Use the Google Cloud Console to create and set up your Cloud project: [Go to App Engine](https://console.cloud.google.com/projectselector/appengine/create?lang=java&st=true&_ga=2.128031500.1019080607.1620674798-469289117.1620674798)
    - Select or create a new Cloud project with the name **webee-challenge**.
    - Follow the prompts to ensure that an App Engine application exists and billing is enabled:
        - If you need to create an App Engine application for your project, you are prompted to select the region where you want your App Engine application located.
        - Follow the prompts to create a new billing account or select an existing account.
    - The **Dashboard** opens after your App Engine application has been created and billing has been enabled in your project.
- Initialize google cloud configuration for the project **webee-challenge**: 
```shell
gcloud init
```
- To access Google resources from your project when running locally, set the application default credentials by running:
```shell
gcloud auth application-default login
```

# Run the application

To start the server, execute:

```shell
$ gradle bootRun
```

After that the server start, 
Luego de iniciar el server, se tiene acceso a la documentacino swagger generada: `http://localhost:8080/swagger-ui/`.


## Deploy

To deploy te application to the cloud, execute the follows commands:

```shell
$ export GOOGLE_CLOUD_PROJECT=webee-challenge 
$ gradle appengineDeploy
```

## Use

### Add device

Example to register a new device:

Local execution:

```shell
curl --location --request POST 'http://localhost:8080/device' \
--header 'Content-Type: application/json' \
--data-raw '{
    "macAddress":"FF:FF:FF:FF:FF:FF",
    "timeStamp":"2021-05-11T17:21:17"
}'
```

Remote :

```shell
curl --location --request POST 'https://webee-challenge.rj.r.appspot.com/device' \
--header 'Content-Type: application/json' \
--data-raw '{
    "macAddress":"FF:FF:FF:FF:FF:FF",
    "timeStamp":"2021-05-11T17:21:17"
}'
```

### Get all device

Local execution:

```shell
curl --location --request GET 'http://localhost:8080/device?page=0&size=10'
```

Remote :

```shell
curl --location --request GET 'http://webee-challenge.rj.r.appspot.com/device?page=0&size=10'
```

### Search by mac address

Local execution:

```shell
curl --location --request GET 'http://localhost:8080/device/search?macAddress=FF:FF:FF:FF:FF:FF'
```

Remote :

```shell
curl --location --request GET 'http://webee-challenge.rj.r.appspot.com/device/search?macAddress=FF:FF:FF:FF:FF:FF'
```

### Search by ID

Local execution:

```shell
curl --location --request GET 'http://localhost:8080/device/search?id=5079418695319552' 
```

Remote :

```shell
curl --location --request GET 'http://webee-challenge.rj.r.appspot.com/device/search?id=5079418695319552' 
```

### Delete

Local execution:

```shell
curl --location --request DELETE 'http://localhost:8080/device/5071211717459968'
```

Remote :

```shell
curl --location --request DELETE 'http://webee-challenge.rj.r.appspot.com/device/5071211717459968'
```






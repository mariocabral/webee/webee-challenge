package com.marioc.webee.challenge.controller;

import com.marioc.webee.challenge.model.Device;
import com.marioc.webee.challenge.repository.DeviceRepository;
import com.marioc.webee.challenge.service.DeviceService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DeviceControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private DeviceRepository deviceRepository;

    @InjectMocks
    private DeviceService deviceService;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void postNewDevide() throws Exception {
        Device d = new Device("FF:FF:FF:FF:FF:FF", dateFormat.parse("2021-05-11T17:21:17"));
        Device storedDevice = new Device("FF:FF:FF:FF:FF:FF", dateFormat.parse("2021-05-11T17:21:17"));
        storedDevice.setId(1111L);
        when(deviceRepository.save(ArgumentMatchers.any())).thenReturn(storedDevice);

        ResponseEntity<Device> response = restTemplate.postForEntity(
                new URL("http://localhost:" + port + "/device").toString(), d, Device.class);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(d.getMacAddress(), response.getBody().getMacAddress());
        Assertions.assertEquals(d.getTimeStamp(), response.getBody().getTimeStamp());
        Assertions.assertNotNull(response.getBody().getId());
    }

    @Test
    void postNewDevideInvalidMacAddress() throws Exception {
        Device d = new Device("FF:FF:FF:FF:FF", dateFormat.parse("2021-05-11T17:21:17"));
        ResponseEntity<Error> response = restTemplate.postForEntity(
                new URL("http://localhost:" + port + "/device").toString(), d, Error.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assertions.assertEquals("MAC Address not have expected format", response.getBody().getMessage());
    }

    @Test
    void postNewDevideInvalidTimeStamp() throws Exception {
        Device d = new Device("FF:FF:FF:FF:FF:FF", dateFormat.parse("2019-05-11T17:21:17"));
        ResponseEntity<Error> response = restTemplate.postForEntity(
                new URL("http://localhost:" + port + "/device").toString(), d, Error.class);
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assertions.assertEquals("Time stamp must be after of 2020-01-01", response.getBody().getMessage());
    }

    @Test
    void getDevices() throws ParseException, MalformedURLException {
        List<Device> result = getDeviceList();
        Page<Device> p = new PageImpl<>(result);
        when(deviceRepository.findAll(PageRequest.of(1,10))).thenReturn(p);

        ResponseEntity<Page> response = restTemplate.getForEntity(
                new URL("http://localhost:" + port + "/device?page=0&size=10").toString(), Page.class);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private List<Device> getDeviceList() throws ParseException {
        List<Device> result = new LinkedList<>();
        for (int i = 0; i <9; i++) {
            Device storedDevice = new Device("FF:FF:FF:FF:FF:F" + i , dateFormat.parse("2021-05-11T17:21:17"));
            storedDevice.setId(Integer.toUnsignedLong(i));
            result.add(storedDevice);
        }
        return result;
    }

    @Test
    void getDeviceByMacAddress() throws MalformedURLException, ParseException {
        Device storedDevice = new Device("F1:FF:FF:FF:FF:FF", dateFormat.parse("2021-05-11T17:21:17"));
        storedDevice.setId(1111L);
        when(deviceRepository.findByMacAddress(ArgumentMatchers.any())).thenReturn(java.util.Optional.of(storedDevice));

        ResponseEntity<Device> response = restTemplate.getForEntity(
                new URL("http://localhost:" + port + "/device/search?macAddress=F1:FF:FF:FF:FF:FF").toString(), Device.class);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getDeviceById() throws MalformedURLException, ParseException {
        Device storedDevice = new Device("F1:FF:FF:FF:FF:FF", dateFormat.parse("2021-05-11T17:21:17"));
        storedDevice.setId(1111L);
        when(deviceRepository.findById(ArgumentMatchers.any())).thenReturn(java.util.Optional.of(storedDevice));

        ResponseEntity<Device> response = restTemplate.getForEntity(
                new URL("http://localhost:" + port + "/device/search?id=1111").toString(), Device.class);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getDeviceByMacAddressAndId() throws MalformedURLException, ParseException {
        Device storedDevice = new Device("F1:FF:FF:FF:FF:FF", dateFormat.parse("2021-05-11T17:21:17"));
        storedDevice.setId(1111L);
        when(deviceRepository.findByMacAddressAndId(ArgumentMatchers.any(),ArgumentMatchers.any())).thenReturn(java.util.Optional.of(storedDevice));

        ResponseEntity<Device> response = restTemplate.getForEntity(
                new URL("http://localhost:" + port + "/device/search?macAddress=F1:FF:FF:FF:FF:FF&id=1111").toString(), Device.class);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void deleteDeviceById() throws MalformedURLException {
        doNothing().when(deviceRepository).deleteById(ArgumentMatchers.any());
        restTemplate.delete(
                new URL("http://localhost:" + port + "/device/1111").toString());
        verify(deviceRepository).deleteById(1111L);
    }
}
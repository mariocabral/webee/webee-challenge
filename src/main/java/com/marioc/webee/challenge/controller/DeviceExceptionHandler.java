package com.marioc.webee.challenge.controller;

import com.marioc.webee.challenge.exception.DeviceNotFoundException;
import com.marioc.webee.challenge.exception.DeviceValidationException;
import com.marioc.webee.challenge.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Date;

@ControllerAdvice
public class DeviceExceptionHandler {

    @ResponseBody
    @ExceptionHandler(DeviceNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorMessage deviceNotFoundHandler(DeviceNotFoundException ex) {
        ErrorMessage error = new ErrorMessage(new Date(),HttpStatus.BAD_REQUEST.value(), "Device not found", ex.getMessage());
        return error;
    }

    @ResponseBody
    @ExceptionHandler(DeviceValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorMessage deviceValidationHandler(DeviceValidationException ex) {
        ErrorMessage error = new ErrorMessage(new Date(),HttpStatus.BAD_REQUEST.value(), "Invalid device data", ex.getMessage());
        return error;
    }
}



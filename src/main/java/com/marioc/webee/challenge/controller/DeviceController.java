package com.marioc.webee.challenge.controller;

import com.marioc.webee.challenge.exception.DeviceNotFoundException;
import com.marioc.webee.challenge.exception.SearchDeviceException;
import com.marioc.webee.challenge.model.Device;
import com.marioc.webee.challenge.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/device")
@Slf4j
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @PostMapping
    public Device postNewDevide(@RequestBody Device device){
        log.info("Call post Device");
        Device result = deviceService.addNewDevice(device);
        return result;
    }

    @GetMapping
    public Page<Device> getDevices(@RequestParam(name = "page") int page, @RequestParam(name = "size") int size){
        log.info("get all Device");
        Page<Device> result = deviceService.getAllDevices(PageRequest.of(page, size));
        return result;
    }

    @GetMapping("/search")
    public Device searchDevice(@RequestParam(name="macAddress", required = false) String macAddress, @RequestParam(name="id", required = false) Long id){
        log.info("search Device");
        if (!ObjectUtils.isEmpty(macAddress) && !ObjectUtils.isEmpty(id)){
            Optional<Device> result = deviceService.getDeviceByMacAddressAndId(macAddress, id);
            return result.orElseThrow(()-> new DeviceNotFoundException(macAddress, id));
        } else if (!ObjectUtils.isEmpty(macAddress)){
            Optional<Device> result = deviceService.getDeviceByMacAddress(macAddress);
            return result.orElseThrow(()-> new DeviceNotFoundException(macAddress));
        } else if ( !ObjectUtils.isEmpty(id)){
            Optional<Device> result = deviceService.getDeviceById(id);
            return result.orElseThrow(()-> new DeviceNotFoundException(id));
        }
        throw new SearchDeviceException("Search device require macAddress or id parameters");
    }

    @DeleteMapping("/{id}")
    public void deleteDeviceById(@PathVariable(name="id") Long id){
        log.info("delete Device by id");
        deviceService.deleteDeviceById(id);
    }

}

package com.marioc.webee.challenge.model;

import lombok.Data;

import javax.management.ConstructorParameters;
import java.util.Date;

@Data
public class ErrorMessage {
    private Date timestamp;
    private int status;
    private String error;
    private String message;

    public ErrorMessage(Date timestamp, int status, String error, String message) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
    }
}

package com.marioc.webee.challenge.model;

import lombok.Data;
import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.cloud.gcp.data.datastore.core.mapping.Field;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Entity(name = "devices")
public class Device {
    @Id
    @Field(name = "device_id")
    private Long id;
    private String macAddress;
    private Date timeStamp;

    public Device(String macAddress, Date timeStamp) {
        this.macAddress = macAddress;
        this.timeStamp = timeStamp;
    }
}

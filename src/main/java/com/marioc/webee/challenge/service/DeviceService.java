package com.marioc.webee.challenge.service;

import com.marioc.webee.challenge.exception.DeviceValidationException;
import com.marioc.webee.challenge.model.Device;
import com.marioc.webee.challenge.repository.DeviceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;

import java.util.Date;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Value("#{new java.text.SimpleDateFormat(\"${webee.date.format}\").parse(\"${webee.device.min.timestamp}\")}")
    private Date deviceMinTimeStamp;

    @Value("${webee.mac.address.format}")
    private String macAddressFormat;

    public Device addNewDevice(Device device) {
        log.info("Storing new device: " + device);
        validate(device);
        return deviceRepository.save(device);
    }

    public void validate(Device device) {
        if (isInvalidMacAddress(device.getMacAddress())) {
          throw new DeviceValidationException("MAC Address not have expected format");
        }
        if (isInvalidTimeStamp(device.getTimeStamp())) {
            throw new DeviceValidationException ("Time stamp must be after of 2020-01-01");
        }
    }

    private boolean isInvalidMacAddress(String macAddress){
        Pattern p = Pattern.compile(macAddressFormat);
        if (ObjectUtils.isEmpty(macAddress)){
            return false;
        }
        Matcher m = p.matcher(macAddress);
        return !m.matches();
    }

    private boolean isInvalidTimeStamp(Date timeStamp){
        return deviceMinTimeStamp.after(timeStamp);
    }

    public Page<Device> getAllDevices(PageRequest pageRequest){
        return deviceRepository.findAll(pageRequest);
    }


    public Optional<Device> getDeviceByMacAddress(String macAddress) {
        return deviceRepository.findByMacAddress(macAddress);
    }

    public Optional<Device> getDeviceById(Long id) {
        return deviceRepository.findById(id);
    }

    public void deleteDeviceById(Long id) {
        deviceRepository.deleteById(id);
    }

    public Optional<Device> getDeviceByMacAddressAndId(String macAddress, Long id) {
        return deviceRepository.findByMacAddressAndId(macAddress, id);
    }
}

package com.marioc.webee.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebeeChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebeeChallengeApplication.class, args);
	}

}

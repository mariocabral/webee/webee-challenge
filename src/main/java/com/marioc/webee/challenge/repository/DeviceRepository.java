package com.marioc.webee.challenge.repository;

import com.marioc.webee.challenge.model.Device;
import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;

import java.util.Optional;

public interface DeviceRepository  extends DatastoreRepository<Device, Long> {

    Optional<Device> findByMacAddress(String macAddress);


    Optional<Device> findByMacAddressAndId(String macAddress, Long id);
}

package com.marioc.webee.challenge.exception;

public class DeviceValidationException extends RuntimeException{

    public DeviceValidationException(String message) {
        super(message);
    }
}

package com.marioc.webee.challenge.exception;

public class SearchDeviceException extends RuntimeException{
    public SearchDeviceException(String message) {
        super(message);
    }
}

package com.marioc.webee.challenge.exception;

public class DeviceNotFoundException extends RuntimeException{
    public DeviceNotFoundException(String macAddress) {
        super("Device with mac address:" + macAddress + " not found.");
    }

    public DeviceNotFoundException(Long id) {
        super("Device with id:" + id + " not found.");
    }

    public DeviceNotFoundException(String macAddress, Long id) {
        super("Device with id:" + id + " and maccAddress:" + macAddress +" not found.");
    }
}
